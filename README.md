# Remarks
* The prerquisite for the Free Binding Energy estimation is a Molecular Docking clculation
* The most favourable pose is considred as a starting point for a Molecular Dynamics calculation
* The estimtion of the ligand binding will be carried out with the following methodolgies
    * QMMM with various variants
    * Alchemical approached
    * The initial Molecular Docking result will be also considered as a first estimate/guess
* Chosen systems
    * Receptor: 3l0j
    * Ligands: Digoxin, AZ5104
* The issues to be discussed
    * The experimental binding estimation (defined "somehow") as the reference
    * To which extent can we trust the chepest possible method, i.e. the Molecular Docking approach?
    * Does gradual increase of the complexity of the QMMM approach increases the quality of obtained binding energy?
    * How about the DFTB? Is this method capable of delivering satisfactory results?
    * How different is the Molecular Docking prediction of Fred (OpenEye, licensed; free for Academia), rDock (free) and Dock (free) in comparison to Autodock (assuming for a while that Autodock is a "reference" docking method)? 
* The tools 
    * In principle the idea is to use the Python ecosystem
    * Openforcefield for the Molecular Dynamics preparation
    * OpenMM for Molecular Dynamics
    * Autodock, rDock, DOCK wrapped in Python
    * OpenEye toolkits
    * Alchemistry from Omnia
    * Auxiliary stuff: parmed, mdtraj, PDBfixer