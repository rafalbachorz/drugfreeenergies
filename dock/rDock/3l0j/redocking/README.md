* cavity creation
```
rbcavity -r 3l0j_rdock.prm -was > 3l0j_cavity.log
```

* docking
```
rbdock -r 3l0j_rdock.prm -p dock.prm -n 100 -i Ligand_noWater_allHydrogensH.sd -o 3l0j_docking_out > 3l0j_docking_out.log
```

* sorting
```
sdsort -n -f'SCORE' 3l0j_docking_out.sd > 3l0j_docking_out_sorted.sd
```

* rmsd check
```
sdrmsd Ligand_noWater_allHydrogensH.sd 3l0j_docking_out_sorted.sd
```

* https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4112621/

* pocket location: https://github.com/Discngine/fpocket