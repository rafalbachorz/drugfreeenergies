#!/bin/bash
receptor=$1
ligand=$2
cenOfMass=`python /home/worker1/MD/drugfreeenergies/dock/AD/scripts/center_of_mass.py $receptor.pdb`
echo Center of mass: $cenOfMass

pythonsh /QC/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_receptor4.py -A "hydrogens" -r $receptor.pdb -v
pythonsh /QC/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_ligand4.py -l $ligand.mol2 -v
pythonsh /QC/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_gpf4.py -l $ligand.pdbqt -r $receptor.pdbqt -p gridcenter=$cenOfMass -p npts='126,126,126' -o $receptor_$ligand.gpf -v
pythonsh /QC/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_dpf4.py -l $ligand.pdbqt -r $receptor.pdbqt -p ga_num_evals=10000000 -o $receptor_$ligand.dpf -v

echo Starting autogrid
autogrid4 -p $receptor_$ligand.gpf -l $receptor_$ligand.gpl
echo Starting autodock
autodock4 -p $receptor_$ligand.dpf -l $receptor_$ligand.dpl
echo DONE!
