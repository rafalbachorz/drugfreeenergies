from pypdb import *
from pdbfixer import PDBFixer
from simtk.openmm.app import PDBFile

def getPDBContent(pdbId):
    return get_pdb_file(pdbId, filetype='pdb', compression=False)

def storePDBContent(content, fileName):
    with open(fileName, 'w') as f:
        f.write(content)

def cleanPDBfile(fileName):

    fixer = PDBFixer(filename=fileName)
    fixer.findMissingResidues()
    fixer.findNonstandardResidues()
    fixer.removeHeterogens(False)
    fixer.findMissingAtoms()
    #fixer.addMissingAtoms()

    fileNameClean = fileName.split('.')[0]
    fileNameClean = fileNameClean+'_clean.pdb'
    PDBFile.writeFile(fixer.topology, fixer.positions, open(fileNameClean, 'w'))

if __name__ == '__main__':

    import argparse
    from tabulate import tabulate
    from pathlib import Path

    parser = argparse.ArgumentParser(
             description='PDN retriever/cleaner.',
             epilog='Example:\n',
             formatter_class=argparse.RawTextHelpFormatter
                                                        )

    parser.add_argument('pdbId')
    #parser.add_argument('-p', action='store', dest='printMode', default='all', help='Mode of printing; all - all poses; best - only best pose')
    parser.add_argument('-v', '--version', action='version', version='getAndClean.py v. 1.0')

    args = parser.parse_args()
    
    
    pdbContent = getPDBContent(args.pdbId)
    storePDBContent(pdbContent, args.pdbId+'.pdb')
    cleanPDBfile(args.pdbId+'.pdb')
