currDir=`pwd`
ligand=$1
receptors=$2
#all
#for receptor in 1sr7 1x7j 1xvp 2aa2 3ax8 3ipq 3l0e 3l0j 3l1b 3r8d 3uud 4ql8 5nfp
#all seamless
#for receptor in 1sr7 1x7j 1xvp 2aa2 3ax8 3ipq 3l0e 3l0j 3l1b 3uud 4ql8 5nfp
# dockAll.sh ligand [receptor1, receptor2]
for receptor in $receptors
do
 echo $receptor
 if [[ ! -e $receptor ]]; then
   mkdir $receptor
 fi
 cd $receptor
 receptorFile=${receptor}_clean.pdb
 echo $receptorFile
 cp $currDir/receptors/$receptorFile .
 cp $currDir/ligands/$ligand.mol2 .
 $currDir/scripts/runDocking.sh ${receptor}_clean $ligand
 cd $currDir
done
