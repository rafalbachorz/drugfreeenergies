currDir=`pwd`
ADfile=$1
for receptor in 1skx 1sr7 1x7j 1xvp 2aa2 3ax8 3ipq 3l0e 3l0j 3l1b 3uud 4ql8 5nfp
do
 echo $receptor `python scripts/parseAutoDockFiles.py $receptor/$ADfile -p best`
done
