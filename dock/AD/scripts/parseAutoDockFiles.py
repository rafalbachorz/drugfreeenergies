from Bio.PDB import *
from Bio import *
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import PPBuilder

import re

def findPattern(path, pattern):
    foundLine = None
    for i, line in enumerate(open(path.as_posix())):
        findings = re.findall(pattern, line) 
        if (findings):
            foundLine = i
    return foundLine

def getPieceOfFile(path, line1, line2):
    chosenLines = list(range(line1, line2))
    returnList = []
    with open(path.as_posix(), 'rt') as file:
        lines = file.readlines()
        for desiredLine in range(line1, line2):
            returnList.append(lines[desiredLine])
    return returnList

def extractResults(linesChunk):
    pattern = re.compile("^...[1-9].*")
    results = []
    for line in linesChunk:
        interestingLine = re.findall(pattern, line)
        if interestingLine:
            results.append(interestingLine[0])
    resultsPostprocessed = {'eneLow': [], 'eneAve': [], 'count': []}
    for line in results:
        resultsPostprocessed['eneLow'].append(line.split("|")[1].strip())
        resultsPostprocessed['eneAve'].append(line.split("|")[3].strip())
        resultsPostprocessed['count'].append(line.split("|")[4].strip())
    return resultsPostprocessed

def getMacromoleculeBox(PDBfile):
    print("Processed receptor: "+PDBfile)
    parser = PDBParser(QUIET = True) 
    macromolecule = parser.get_structure(PDBfile,PDBfile) 
    for model in macromolecule.get_models():
        print("model ", model.id)
        extreemeCoords = {}
        coordMin = [999, 999, 999]
        coordMax = [-999, -999, -999]
        for chain in model.get_chains():
            print("chain ", chain.id, len(list(chain.get_residues())), "residues")
            residuesPerChain = []
            for residue in chain.get_residues():
                for atom in residue.get_atoms():
                #print(residue.get_resname(), atom.get_name(), atom.get_coord())
                    coords = atom.get_coord()

                    for iii, (cooMin, cooMax, coo) in enumerate(zip(coordMin, coordMax, coords)):
                        coordMin[iii] = min(cooMin, float(coo))
                        coordMax[iii] = max(cooMax, float(coo))

        extreemeCoords[model.id] = [coordMin, coordMax]
    return extreemeCoords

if __name__ == '__main__':
    
    import argparse
    from tabulate import tabulate
    from pathlib import Path

    parser = argparse.ArgumentParser(
        description='AutoDock files parser.',
        epilog='Example:\n',
        formatter_class=argparse.RawTextHelpFormatter
        )
    
    parser.add_argument('ADfile')
    parser.add_argument('-p', action='store', dest='printMode', default='all', help='Mode of printing; all - all poses; best - only best pose')
    parser.add_argument('-v', '--version', action='version', version='parseAutoDockFiles.py v. 1.0')
            
    args = parser.parse_args()
    p = Path(args.ADfile)

    pattern1 = re.compile("^\tCLUSTERING HISTOGRAM")
    pattern2 = re.compile("^\tRMSD TABLE")
    lineBeg = findPattern(p, pattern1)
    lineEnd = findPattern(p, pattern2)
    lines = getPieceOfFile(p, lineBeg, lineEnd)
    if (args.printMode=='all'):
        print('## '+args.ADfile)
        print(tabulate(extractResults(lines), tablefmt="pipe", headers="keys"))
    elif (args.printMode=='best'):
        energy = lines[9].split('|')[1].strip()
        print(energy)
    else:
        print('Unrecognized printing mode...')

