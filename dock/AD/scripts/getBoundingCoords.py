from Bio.PDB import *
from Bio import *
from Bio.PDB.PDBParser import PDBParser
from Bio.PDB.Polypeptide import PPBuilder

def getMacromoleculeBox(PDBfile):
    print("Processed receptor: "+PDBfile)
    parser = PDBParser(QUIET = True) 
    macromolecule = parser.get_structure(PDBfile,PDBfile) 
    for model in macromolecule.get_models():
        print("model ", model.id)
        extreemeCoords = {}
        coordMin = [999, 999, 999]
        coordMax = [-999, -999, -999]
        for chain in model.get_chains():
            print("chain ", chain.id, len(list(chain.get_residues())), "residues")
            residuesPerChain = []
            for residue in chain.get_residues():
                for atom in residue.get_atoms():
                #print(residue.get_resname(), atom.get_name(), atom.get_coord())
                    coords = atom.get_coord()

                    for iii, (cooMin, cooMax, coo) in enumerate(zip(coordMin, coordMax, coords)):
                        coordMin[iii] = min(cooMin, float(coo))
                        coordMax[iii] = max(cooMax, float(coo))

        extreemeCoords[model.id] = [coordMin, coordMax]
    return extreemeCoords

if __name__ == '__main__':
    
    import argparse
    
    parser = argparse.ArgumentParser(
        description='Calculates the extreme coordinates of atoms contained in the PDB file.\n'\
                    'By default, all atoms in the PDB file are included in the calculation.',
        epilog='Example:\n'\
                'center_of_mass.py ~/Desktop/protein.pdb \n'\
                '[-8.125, 20.461, -10.438]'
                '\n\nNote that for the center of mass calculation, the relative\natomic'\
                ' weights are taken into account (atomic mass unit [u]).\n\n'\
                'A list of the atomic weights can be found, e.g., at\n'\
                'http://en.wikipedia.org/wiki/List_of_elements',
        formatter_class=argparse.RawTextHelpFormatter
        )
    
    parser.add_argument('PDBfile')
    parser.add_argument('-v', '--version', action='version', version='center_of_mass v. 1.0')
            
            
    args = parser.parse_args()
    extremeCoords = getMacromoleculeBox(args.PDBfile)

    print(extremeCoords)

