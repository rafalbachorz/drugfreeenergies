#!/bin/bash
pdbqtfile=$1.pdbqt
molfile=$1.mol2
pdbfile=$1.pdb
prefile=$1.prepi
frcfile=$1.frcmod

residueName=$2

receptorpdb=$3.pdb
complexname=$1_$3

antechamber -i $molfile -fi mol2 -o $molfile -fo mol2 -rn $residueName -nc 0
antechamber -i $molfile -fi mol2 -o $pdbfile -fo pdb -rn $residueName -nc 0
antechamber -i $molfile -fi mol2 -o $prefile -fo prepi
parmchk2 -i $prefile -f prepi -o $frcfile

cat << EOF > tleapScript.in
set default PBradii mbondi2
# load the protein force field
source leaprc.ff14SB.redq
# load in GAFF
source leaprc.gaff
loadamberparams $frcfile
loadamberprep $prefile
LIG = loadpdb $pdbfile
REC = loadpdb $receptorpdb
COM  = combine {REC LIG}

saveamberparm REC $3.prm7 $3.rst7
saveamberparm LIG $1.prm7 $1.rst7
saveamberparm COM $complexname.prm7 $complexname.rst7

savePDB COM $complexname.pdb
quit
EOF

tleap -f tleapScript.in
