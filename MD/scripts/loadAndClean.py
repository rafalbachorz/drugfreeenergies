from pypdb import *
from pdbfixer import PDBFixer
from simtk.openmm.app import PDBFile

def cleanPDBfile(fileNameIn, fileNameOut):

    fixer = PDBFixer(filename=fileNameIn)
    fixer.findMissingResidues()
    fixer.findNonstandardResidues()
    fixer.removeHeterogens(False)
    fixer.findMissingAtoms()
    fixer.addMissingHydrogens(7.0)

    PDBFile.writeFile(fixer.topology, fixer.positions, open(fileNameOut, 'w'))

if __name__ == '__main__':

    import argparse
    from tabulate import tabulate
    from pathlib import Path

    parser = argparse.ArgumentParser(
             description='PDB cleaner.',
             epilog='Example:\n',
             formatter_class=argparse.RawTextHelpFormatter
                                                        )

    parser.add_argument('-pdbIn', required=True)
    parser.add_argument('-pdbOut', required=True)
    #parser.add_argument('-p', action='store', dest='printMode', default='all', help='Mode of printing; all - all poses; best - only best pose')
    parser.add_argument('-v', '--version', action='version', version='loadAndClean.py v. 1.0')

    args = parser.parse_args()
    print(args.pdbIn)
    print(args.pdbOut)
    print(args) 
    
    #pdbContent = getPDBContent(args.pdbId)
    #storePDBContent(pdbContent, args.pdbId+'.pdb')
    cleanPDBfile(args.pdbIn, args.pdbOut)
