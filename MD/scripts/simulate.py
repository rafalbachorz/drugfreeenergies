import sys
# OpenMM Imports
import simtk.openmm as mm
import simtk.openmm.app as app

# ParmEd Imports
from parmed import load_file, unit as u
from parmed.openmm import StateDataReporter, NetCDFReporter, RestartReporter


def simulate(sysName, mSteps, sSteps, frameStorageFreq, nThreads):

    molsystem = load_file(sysName+'.prm7', sysName+'.rst7')

    print('Creating OpenMM System')
    system = molsystem.createSystem(nonbondedMethod=app.NoCutoff,
                                    constraints=app.HBonds, implicitSolvent=app.OBC2,
                                    implicitSolventSaltConc=0.1*u.moles/u.liter
                                    )

    # Create the integrator to do Langevin dynamics
    print('Creating OpenMM Integrator')
    integrator = mm.LangevinIntegrator(
                        300*u.kelvin,       # Temperature of heat bath
                        1.0/u.picoseconds,  # Friction coefficient
                        2.0*u.femtoseconds, # Time step
                        )

    print('Creating OpenMM Platform')
    platform = mm.Platform.getPlatformByName('CPU')
    properties = {}
    properties["Threads"] = str(nThreads);

    # Create the Simulation object
    print('Creating OpenMM Simulation')
    sim = app.Simulation(molsystem.topology, system, integrator, platform, properties)
    # Set the particle positions
    sim.context.setPositions(molsystem.positions)

    # Minimize the energy
    print('Minimizing energy')
    sim.minimizeEnergy(maxIterations=mSteps)

    print('Creating OpenMM Reporters')
    sim.reporters.append(StateDataReporter(sys.stdout, frameStorageFreq, step=True, potentialEnergy=True, volume=True, totalEnergy=True, kineticEnergy=True, temperature=True, density=True))
    sim.reporters.append(app.PDBReporter(sysName+'_trajectory.pdb', frameStorageFreq))
    #sim.reporters.append(NetCDFReporter(sysName+'_trajectory.nc', frameStorageFreq, crds=True))
    sim.reporters.append(app.DCDReporter(sysName+'_trajectory.dcd', frameStorageFreq))

    restrt = RestartReporter(sysName+'_restart.rst7', frameStorageFreq)
    sim.reporters.append(restrt)

    # Run dynamics
    print('Running dynamics')
    sim.step(sSteps)

    state = sim.context.getState(getPositions=True, getVelocities=True, enforcePeriodicBox=False)
    restrt.report(sim, state)

if __name__ == '__main__':
    
    import argparse
    from tabulate import tabulate
    from pathlib import Path

    parser = argparse.ArgumentParser(
        description='OpenMM simulation.',
        epilog='Examplary usage: python simulate.py -sys mysystem -msteps 500 -ssteps 5000 -ffreq 100 -nthreads 8',
        formatter_class=argparse.RawTextHelpFormatter
        )
    
    parser.add_argument('-sys', action='store', dest='sysName', required=True, type=str, help='System name')
    parser.add_argument('-msteps', action='store', dest='mSteps', default=500, type=int, help='Number od minimization steps')
    parser.add_argument('-ssteps', action='store', dest='sSteps', default=5000, type=int, help='Number od simulation steps')
    parser.add_argument('-ffreq', action='store', dest='fFreq', default=100, type=int, help='Frame storage frequency')
    parser.add_argument('-nthreads', action='store', dest='nThreads', default=2, type=int, help='Number of threads')
    parser.add_argument('-v', '--version', action='version', version='parseAutoDockFiles.py v. 1.0')
            
    args = parser.parse_args()

    simulate(args.sysName, args.mSteps, args.sSteps, args.fFreq, args.nThreads)
